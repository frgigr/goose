#ifndef GOOSETABLE_H
#define GOOSETABLE_H

enum class squareType: char 
{
	empty='.',
	goose='G',
	maze='M',
	skeleton='S'
};

class CPlayer;

class CGooseTable {
	public:
		CGooseTable();
		void addPlayer(CPlayer p);
		unsigned short int getTotalPlayers() {return players.size(); };
		unsigned short int ifWonDeclareWinner();
		void print();
		unsigned short int DiceAndUpdatePositions();

	private:
		std::vector<CPlayer> players;
		std::vector<squareType> table;
};
#endif
