#ifndef GOOSEPLAYER_H
#define GOOSEPLAYER_H

class CPlayer {
	public:
		CPlayer(std::string n);
		std::string getPlayerName() {return name; };
		void setPlayerName(std::string pname) {name=pname; };
		unsigned int getPlayerPosition() {return position; };
		void setPlayerPosition(unsigned int pos) {position=pos; };
		unsigned short int rollDiceAndUpdatePosition();

	private:
		std::string name;
		unsigned int position;
};
#endif
