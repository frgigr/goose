GG=g++
OO=-O3
CFLAGS=-Wall -c -I include
BUILD=.build
SRC=src
BIN=bin

$(BIN)/goose: $(BUILD)/main.o $(BUILD)/goosePlayer.o $(BUILD)/gooseTable.o
	@mkdir -p $(BIN)
	$(GG) $(OO) -o $@ $^

$(BUILD)/main.o: $(SRC)/main.cpp
	@mkdir -p $(BUILD)
	$(GG) $(OO) $(CFLAGS) -o $@ $<

$(BUILD)/goosePlayer.o: $(SRC)/goosePlayer.cpp
	$(GG) $(OO) $(CFLAGS) -o $@ $<

$(BUILD)/gooseTable.o: $(SRC)/gooseTable.cpp
	$(GG) $(OO) $(CFLAGS) -o $@ $<

clean:
	@echo "Cleaning up..."
	@echo "rm -rf $(BUILD)/ $(BIN)/"; rm -rf $(BUILD) $(BIN)
