#include <iostream>
#include <vector>
#include <stdlib.h>
#include <time.h>
#include "goosePlayer.h"
#include "gooseTable.h"

int main() {
	std::cout << "Welcome to the Goose Game...Ready to start?" << std::endl;
	srand(time(NULL));

	CPlayer p1("Francesco"), p2("Giorgio"), p3("Altro");

	CGooseTable table;
	table.addPlayer(p1);
	table.addPlayer(p2);
	table.addPlayer(p3);

	std::cout << table.getTotalPlayers() << " players in this game!" << std::endl;

	table.print();

	unsigned short int rounds=0;
	while(rounds==0) {
		rounds=table.DiceAndUpdatePositions();
		table.print();
	}
	std::cout << "Game finished" << std::endl;
	return 0;
}
