#include <iostream>
#include <string>
#include "goosePlayer.h"

CPlayer::CPlayer(std::string n) {
	name = n;
	position = 0;
	std::cout << "New player " << name << " added! Initializing position at " << position << std::endl;
};

unsigned short int CPlayer::rollDiceAndUpdatePosition() {
	unsigned short int dice=rand() % 11 + 2;
	position += dice;

	std::cout << name << " scored " << dice << "! Position is now " << position << std::endl;
	return dice;
};
