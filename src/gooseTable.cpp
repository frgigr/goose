#include <iostream>
#include <string>
#include <vector>
#include "gooseTable.h"
#include "goosePlayer.h"

CGooseTable::CGooseTable() {
	players.clear();

	for(unsigned short int i=0; i<63; i++) {
		switch(i+1) {
			case 5:
			case 9:
			case 18:
			case 27:
			case 36:
			case 45:
			case 54:
				table.push_back(squareType::goose);
				break;
			case 42:
				table.push_back(squareType::maze);
				break;
			case 58:
				table.push_back(squareType::skeleton);
				break;
			default:
				table.push_back(squareType::empty);
		}
	}
};

void CGooseTable::addPlayer(CPlayer p) {
	players.push_back(p);
};

std::ostream& operator<<(std::ostream& os, squareType sq) {
	return os << static_cast<char>(sq);
};

unsigned short int CGooseTable::DiceAndUpdatePositions() {
	unsigned short int result=0;
	std::cout << "\nNOW ROLLING DICE" << std::endl;
	for(unsigned short int i=0; i<players.size(); i++) {
		unsigned int diceScored=players.at(i).rollDiceAndUpdatePosition();
		if(players.at(i).getPlayerPosition()<63) {
			while(players.at(i).getPlayerPosition()<63 && static_cast<char>(table.at(players.at(i).getPlayerPosition()-1))=='G') {
				players.at(i).setPlayerPosition(players.at(i).getPlayerPosition()+diceScored);
				std::cout << "\033[1;32mYou're lucky " << players.at(i).getPlayerName() << ", Goose! Moving " << diceScored << " step forward to position " << players.at(i).getPlayerPosition() << "\033[0m" << std::endl;
			}
			if(static_cast<char>(table.at(players.at(i).getPlayerPosition()-1))=='M') {
				players.at(i).setPlayerPosition(39);
				std::cout << "\033[1;34mOh no " << players.at(i).getPlayerName() << ", Maze! Moving back to position " << players.at(i).getPlayerPosition() << "\033[0m" << std::endl;
			}
			if(static_cast<char>(table.at(players.at(i).getPlayerPosition()-1))=='S') {
				players.at(i).setPlayerPosition(1);
				std::cout << "\033[1;31mAAAAAAHHHHH " << players.at(i).getPlayerName() << ", Skeleton! Falling back to position " << players.at(i).getPlayerPosition() << "\033[0m" << std::endl;
			}
		}
	}
	result = ifWonDeclareWinner();
	return result;
}

unsigned short int CGooseTable::ifWonDeclareWinner() {
	unsigned short int winners=0;
	for(unsigned short int i=0; i<players.size(); i++) {
		if(players.at(i).getPlayerPosition()>62) {
			winners++;
			std::cout << "\033[1;33mPlayer " << players.at(i).getPlayerName() << " won!\033[0m" << std::endl;
		}
	}
	return winners;
}

void CGooseTable::print() {
	std::cout << "----------------------------------------------------------------------" << std::endl;
	std::cout << " ";
	for(unsigned short int i=1; i<64; i++) {
		if(i==0) std::cout << " ";
		(i%10==0 && i!=0) ? std::cout << i/10 : std::cout << " ";
	}
	std::cout << std::endl;
	std::cout << " ";
	for(unsigned short int j=1; j<64; j++) {
		(j%10!=0) ? std::cout << j%10 : std::cout << '0';
	}
	std::cout << std::endl;
	for(unsigned short int k=0; k<63; k++) {
		squareType value = table.at(k);
		(k==0) ? std::cout << ' ' << value : std::cout << value;
	}
	std::cout << std::endl;
	for(unsigned short int i=0; i<players.size(); i++) {
		if(players.at(i).getPlayerPosition()==0) std::cout << players.at(i).getPlayerName()[0] << std::endl;
	    else std::cout << std::string(players.at(i).getPlayerPosition(), ' ') << players.at(i).getPlayerName()[0] << std::endl;
	}
	std::cout << "----------------------------------------------------------------------" << std::endl;
};
